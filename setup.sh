#!/bin/sh

dirpath="$( cd "$( dirname "$( realpath "$0" )" )" && pwd )"
envfile="$dirpath/.env"

# FFMPEG install
ffmpeg_dest="$(mktemp -d)/ffmpeg.tar.gz"
echo "** Setting up FFMPEG..."
wget -O "$ffmpeg_dest" "https://johnvansickle.com/ffmpeg/releases/ffmpeg-release-amd64-static.tar.xz" || exit $?
tar -xf "$ffmpeg_dest" -C "$dirpath/bundle" || exit $?
FFMPEG_PATH="$dirpath/bundle/$( ls -rt "$dirpath/bundle/" | grep ffmpeg | tail -1 )"

# yt-dlp install
YTDL_EXEC="$dirpath/bundle/yt-dlp_linux"
echo "** Setting up yt-dlp..."
wget -O "$YTDL_EXEC" "https://github.com/yt-dlp/yt-dlp/releases/latest/download/yt-dlp_linux" || exit $?
chmod +x "$YTDL_EXEC"

# .env file
test -e "$envfile" && cp "$envfile" "$dirpath/.env.old"
echo "** New .env file:"
tee "$envfile" << EOF
FFMPEG_PATH="$FFMPEG_PATH"
YTDL_EXEC="$YTDL_EXEC"
EOF
