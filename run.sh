#!/bin/sh

dirpath="$( cd "$( dirname "$( realpath "$0" )" )" && pwd )"
. "$dirpath/.env" || exit $?

LIST_FILE="${LIST_FILE:-$dirpath/.channels-list}"
LOGS_DIR="${LOGS_DIR:-$dirpath/logs}"

test -e "$LOGS_DIR/" || {
    mkdir -p "$LOGS_DIR" || exit $?
}

while read -r line; do
    echo "[$(date)] Starting check of ""$line"""
    "$dirpath/downloader.sh" "$line" >> "$LOGS_DIR/channel-$line.log" 2>&1 &
done < "$LIST_FILE"
