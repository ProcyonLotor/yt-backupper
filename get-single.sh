#!/bin/sh

dirpath="$(dirname "$(realpath "$0")")"
. "$dirpath/.env" || exit $?

script_pid="$( pgrep -o -f "$0 $1" )"
if [ ! "$script_pid" = "$$" ]; then
    echo "[$(date)] ** ERROR: This worker is already running with PID=$script_pid"
    exit 1
fi

video_id="$1"
if [ -z "$video_id" ]; then
    echo "[$(date)] ** ERROR: Missing video ID argument"
    exit 1
fi

TEMP_DIR="${TEMP_DIR:-$(mktemp -d)}"
test -e "$TEMP_DIR/" || {
    mkdir -p "$TEMP_DIR" || exit $?
}

OUTPUT_DIR="${OUTPUT_DIR:-$dirpath/output}/_singles"
test -e "$OUTPUT_DIR/" || {
    mkdir -p "$OUTPUT_DIR" || exit $?
}

if [ -n "$FFMPEG_PATH" ]; then
    export PATH="${FFMPEG_PATH}:${PATH}"
fi

echo "[$(date)] Working on ""$video_id""..."
# Downloads video with:
#  - Best quality video track
#  - All audio tracks with codec=opus, average bitrate of 130KBit/s, sampling rate of 48kHz
#  - All subtitles of all languages (except live chat), embeds them into the file
#  - All chapter markers, embeds them into the file
#  - Video's thumbnail, embeds it into the file
#  - Merges everything into a .mp4 file
#  - Video's description is embeded into file's "comment" field
"${YTDL_EXEC:-yt-dlp}" \
    -i \
    --paths "temp:$TEMP_DIR" \
    --paths "home:$OUTPUT_DIR" \
    --format "bv*+mergeall[vcodec=none][format_id^=251]" \
    --audio-multistreams \
    --parse-metadata "description:(?s)(?P<meta_comment>.+)" \
    --embed-metadata \
    --sub-langs all,-live_chat \
    --embed-subs \
    --embed-chapters \
    --embed-thumbnail \
    --merge-output-format "mp4" \
    -o "%(upload_date>%Y-%m-%d)s__%(title)s__%(id)s.%(ext)s" \
    $YTDL_ARGS \
    "https://www.youtube.com/watch?v=${video_id}" \
&& {
    echo "[$(date)] Done."
    exit 0
} || {
    echo "[$(date)] ** ALERT! Something wrong happaned during download, check the output above."
    exit 1
}
